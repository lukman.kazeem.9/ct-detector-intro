FFE Coding Test Introduction
============================
The test is a debugging exercise. You will be given a C program that simulates a
very simple optical beam smoke detector with a test harness that runs some unit
tests. The program has various bugs that mean the tests fail, and the task is to
identify the problems and fix them. Since the test is time-limited we don’t
necessarily expect you to find all of the problems, but once the test is over we
would like you to tell us what issues you found, how you went about it, how you
found the exercise, and any comments you have on the test program.

To prepare for the test all you need is an environment where you can compile
and debug C programs. We provide the source code files and a Makefile. We
developed the test using GCC as a compiler, so that would be preferable, but any
C compiler should work.

If you are using Windows with MinGW as your environment be aware that we
previously had an issue where a candidate had problems because the program uses
the function getline() from stdlib.h, which wasn’t present in a minimal
installation, and it took them a little time to install the necessary option. On
Linux this would not be an issue.

To avoid issues like this, and to better help candidates prepare, we now provide
this archive which contains a skeleton of the program so that you can confirm
that you have a suitable development environment and will be able to compile and
run the test program when we give it to you at the beginning of the test. This
skeleton program uses the same structure and Makefile, but does  not do anything
apart from iterate over the lines in the unit test file. If you can compile and
run this program, you will be able to compile the actual test code.

The format for the test will be as follows:

1) A 15-minute introduction where we explain the task and give an overview of
how the program works. The actual code will be provided by e-mail at this time.

2) Up to 90 minutes to work on the exercise (someone will be available to answer
questions at any time). You can stop whenever you want, 90 minutes is the limit.

3) A 15-minute discussion for you to explain what you found, how you went about
it, and how you found the exercise. We also ask you to send us a copy of any
modifications you made to the code.

The rest of this file is a more detailed explanation of the test program, which
you can review in advance of the test if you wish. We will go through all of
this information during the introduction, and you will have the opportunity to
ask questions if anything is unclear.

Overview
========
The program is a simplified simulation of a beam smoke detector. These
detectors shine a beam of infra-red light across an area which is bounced off
a reflector on the opposite wall and detected by an IR sensor. Smoke will
obscure the beam, causing the reflected signal to gradually reduce in
strength, and when the detector sees this happening it will signal a 'fire' to
the fire panel to which it is connected. A sudden, rather than gradual
interruption to the beam will cause a 'fault' to be signalled, because this
typically indicates that some obstruction has blocked the path of the beam.

Outputs
-------
The simulated hardware has three outputs that signal the state of the system,
a Fire relay, a Fault relay and an LED which can be red, amber or green. The
relays indicate the state of the system to the connected fire panel, and are
normally both open. In the case of a fire, the Fire relay closes and in the
case of a fault the Fault relay closes. It is not possible for both relays to
be closed at the same time. The LED indicates the state of the relays, green
when they are open, red when the Fire relay is closed and amber when the
Fault relay is closed.

Inputs
------
The system has five inputs, the IR sensor which reports the strength of the
received IR signal as a level between 0 and 100, and four buttons (Reset,
Test, Fire and Fault). The buttons work as follows:
- Pressing the Reset button resets the system.
- Pressing the Test button puts the system into the 'Test' state.
- Pressing the Fire button in 'Test' state tests the fire signalling by
closing the Fire relay. It is also used to clear a Fire condition.
- Pressing the Fault button in 'Test' state tests the fault signalling by
closing the Fault relay.
The Test, Fire and Fault buttons are all toggles, pressing them a second time
reverses the action.
The system will exit 'Test' state if no buttons are pressed for five seconds.

Operation
---------
The 'state_diagram.pdf' file illustrates the internal state machine. The key
points to note are:

- If any buttons are held down when the system starts this is treated as fault.
- If the Reset button is pressed at any time the system resets to its initial
state.
- A Fire condition is defined as the sensor value dropping below 20 and staying
there for over three seconds.
- The Fire condition can be cancelled by pressing the Fire button, as long as
the sensor value has recovered to above 20.
- A Fault condition is defined as the sensor value dropping by more than 30
units in under two seconds
- The Fault condition will automatically clear when the sensor has recovered to
above 50.

Implementation
--------------
This simulated detector has been implemented in ANSI C and incorporates a
suite of unit tests.

The file 'detector.c' contains the whole state machine.
The file 'test.c' contains a test harness. This has the main() function.
The file 'unit_tests.csv' contains the unit tests.

On each line of the unit tests file the first five values are those of the
inputs (Sensor, Test button, Fire button, Fault button, Reset button,
respectively) and the final three values (Fire relay, Fault relay, LED)
are the expected values of the outputs after the state machine has run.
Each line represents one second of elapsed time.

The test harness reads each line of the CSV file, sets the state of each of the
inputs, runs the state machine, then checks that the state of the outputs is
correct (the outputs match the values expected in that line of the test file).

Lines in the CSV file that are prefixed with a '#' are comments and will be
printed to stdout but otherwise ignored.

Note
----
All of the tests in the CSV file are correct, as is the information in this
file and the state diagram. You cannot rely on anything else to be correct as
the program contains a number of errors.
