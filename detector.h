#include <stdbool.h>

enum ledstate {Off, Red, Amber, Green};

struct system_state
{
   // Inputs - for buttons true means pressed.
   unsigned int sensor;
   bool test_button;
   bool fire_button;
   bool fault_button;
   bool reset_button;
   // Outputs - for relays false means open, true means closed.
   bool fire_relay;
   bool fault_relay;
   enum ledstate led;
};

void detector_run(struct system_state *state);
